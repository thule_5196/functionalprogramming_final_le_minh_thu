-- Project: Functional Programing Final Project
-- Created By: Le Minh Thu on 30/04/2016
-- File name: FPFinal.hs

-- import needed library and link to core logic code
import GESolver
import OtherSimpleSolvers
import Control.Monad  
import System.IO

-- main app containing Input and Output
main=do
 putStrLn "Welcome to Your math Helper App"
 putStrLn "1,2 for gaussian Elmination, 3 for quardric equation, 4 cubic equation, 5 for linear equation"
 putStrLn "Please Select Mode: 1 .Read from file, 2 Enter from keyboard| 3. Solve a simple quardric Equation|4. cubic equation| 5.  linear equation"
 modeSelect<- getLine
 if (modeSelect=="1") then do -- module GESolver, input from file
  putStrLn "Enter file name"
  fName<-getLine
  handle <- openFile fName ReadWriteMode
  contents<-hGetContents handle
  putStrLn "The solution of the equation in the file is: "
  putStrLn $show $ (solve (map (map (\a->read a :: Int)) (map words (lines contents))))
  appendFile fName "\r\n"
  appendFile fName (show $ (solve (map (map (\a->read a :: Int)) (map words (lines contents)))))  
  hClose handle 
 else if (modeSelect=="2") then do -- module GESolver, input from keyboard
  putStrLn "Please Enter your number of lines of Equation"
  amount <- getLine
  rows <- forM [1..(read amount)] (\a -> do  
    putStrLn $ "Enter the"++show a++" matrix "  
    line <- getLine  
    return line)  
  putStrLn $ show (solve (map (map (\a->read a :: Int)) (map words rows)))
 else if(modeSelect =="3") then do -- solve quardric equation, input from keyboard
  putStrLn "Please Enter a: "
  a<-getLine
  putStrLn "Please Enter b: "
  b<-getLine
  putStrLn "Please Enter c: "
  c<-getLine
  putStrLn $ show $ solve_quardric_equation (read a ::Double) (read b::Double) (read c::Double)
 else if(modeSelect =="4") then do -- solve cubic equation, input from keyboard
  putStrLn "Please Enter a: "
  a<-getLine
  putStrLn "Please Enter b: "
  b<-getLine
  putStrLn "Please Enter c: "
  c<-getLine
  putStrLn "Please Enter d: "
  d<-getLine
  putStrLn $ show $ solve_cubic_equation (read a ::Double) (read b::Double) (read c::Double) (read d::Double)
 else do -- solve linear equation, input from keyboard
  putStrLn "Please Enter a: "
  a<-getLine
  putStrLn "Please Enter b: "
  b<-getLine
  putStrLn $ show $ solve_linear_equation (read a ::Double) (read b::Double)
 putStrLn "Do you want to continue" -- loop the program while the user still want to
 choice<-getLine
 if (choice `elem` ["Yes","Y","y","1","Yep","yes"]) then main else getLine -- bad Pascal and C# habit of delaying exiting the program

 
 