module ElectronConfig
( thingy
) where
thingy =1
data SubShell = S| P| D | F deriving (Enum,Show, Read ,Eq)
maxCapacity:: SubShell-> Int
maxCapacity a = 2+ fromEnum a * 4

-- configuration z
configuration 0 = ""
configuration z 
 |z<=0 = ""
 | otherwise thingy
