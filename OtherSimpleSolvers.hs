-- Project: Functional Programing Final Project
-- Created By: Le Minh Thu on 04/05/2016
-- File name: FPFinal.hs
module OtherSimpleSolvers(
 solve_linear_equation,
 solve_quardric_equation,
 solve_cubic_equation
) where
-- solve_quardric_equation: takes in 2 number a b and return the result of the equation ax+b=0,
-- return nothing if a=0; whatever b is (b=0-> too many solution,b/=0 no solution at all)
solve_linear_equation:: Double ->Double-> Maybe Double
solve_linear_equation a b
 | a==0 = Nothing
 | otherwise = Just ((-b)/a)
-- solve_quardric_equation: takes in 3 number a b c and return the result of the equation ax2+bx+c=0
solve_quardric_equation:: Double->Double->Double ->Maybe (Double,Double)
solve_quardric_equation a b c 
 |delta < 0 = Nothing
 |otherwise =Just (((-b)+sqrt(delta))/(2*a),((-b)-sqrt(delta))/(2*a))
   where 
    delta = b^2 - 4*a*c
	
-- solve_cubic_equation: takes in 4 number a b c d and return the result of the equation ax3+bx2+cx+d=0	
-- using geonometrical approach
solve_cubic_equation:: Double -> Double-> Double->Double -> [Double]
solve_cubic_equation a b c d
 | (delta ==0) = [(-b+(b^3-27*(a^2)*d)**(1/3))/(3*a)]
 | delta < 0 = [((sqrt (abs delta))/(3*a))*((k+sqrt(k^2+1))**(1/3) + (k-sqrt(k^2+1))**(1/3)) -(b/(3*a))]
 | delta > 0 = if ((abs k)<1) then [(2*(sqrt delta)*(cos ((acos k) / 3))-b)/(3*a),(2*(sqrt delta)*(cos (((acos k) / 3)-(2*pi/3))-b))/(3*a),(2*(sqrt delta)*(cos (((acos k) / 3)+(2*pi/3))-b))/(3*a)] else [((sqrt (abs delta))*(signum k)/(3*a)*((k+sqrt(k^2+1))**(1/3) + (k-sqrt(k^2+1))**(1/3))) -(b/(3*a))]
 where delta= b^2 - 3*a*c
       k=((9*a*b*c)-2*(b^3)-27*(a^2)*d)/ 2 * sqrt ((abs delta) ^3)
 
-- For the cubic equation function: reference
-- Algorithm found in:Advanced and Developed Mathematic Problems for Grade 9 , Vu Huu Binh, 
-- VietNam Educational Publisher, 2007, p.77
--                    Wikipedia: Cubic Equation: https://vi.wikipedia.org/wiki/Ph%C6%B0%C6%A1ng_tr%C3%ACnh_b%E1%BA%ADc_ba
 

 
