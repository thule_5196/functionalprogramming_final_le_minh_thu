-- Project: Functional Programing Final Project
-- Created By: Le Minh Thu on 30/04/2016
-- File name: GESolver.hs
-- module: GESolver (Gaussian Elimination)
module GESolver(
thingy,
swap,
gaussElim,solve)
where
thingy =1

-- type declaration
type Row=[Int] 
type Matrix = [Row]
type Vector =[Int] -- this one mainly act as the set of solution

-- eliminate': lowest level elimination take the first row as the main row, 
-- handle the atomic calculation between two row, substract the result of 
-- two rows which has been rasing to the state of having the same first coefficent
eliminate':: Row -> Row -> Row
eliminate' (xs) (ys) = let c = lcm (head xs) (head ys) 
 in ((zipWith (-) (map (*(c `div` head xs)) xs) (map (*(c`div`head ys)) ys)))

-- swap: function to prevent division by zero happening by 
-- having the first element of the first row of the matrix (or recursive sub-matrix) to be 0
-- if we see any (0:_) row, put it in the end, return when the first occurence of (x:_) (with x!=0) is met
-- if there is no way to swap => set of solution is not having limited set of solution
swap:: Matrix->Matrix
swap [] =error "too many solution"
swap (x:xs)
 |(head x)/=0 =x:xs
 |otherwise = (swap xs)++[x]
 
-- elimHelper: take one row (top row) to be the main row, for each next row,
-- apply the atomic eliminate' with that row that the main row to get a new matrix
-- this could be written inside the gaussElim function in a let in clause or where clause but can also be splited like this
elimHelper:: Row->Matrix ->Matrix
elimHelper r [] = [r]
elimHelper r1 (r:rs)
 |(head r) == 0 = r : (elimHelper r1 rs)
 |otherwise =(eliminate' r1 r):(elimHelper r1 rs)

-- a= elimHelper [1,2,3,10] [[1,1,1,3],[1,-1,1,2]]
-- gaussElim: main function of the module, take in a square matrix and apply gaussian elemination on to it
-- each time of the loop, it will chop away the head of the matrix i.e. we always have a square matrix
-- return: triangle matrix that we can trace back to find the solution
gaussElim:: Matrix->Matrix
gaussElim (r:rs)
 |length r >2 = r: (gaussElim $swap ((map (\a-> tail a) (elimHelper r rs))))
 |otherwise = [r]

 
-- Quick Test
b=gaussElim[[1,2,3,10],[1,1,1,6],[1,-1,1,2]]
-- b=gaussElim [[1,1,1,3],[2,2,1,5],[1,-1,1,1]]
-- Quick Test
c = reverse $ map reverse b

--doBack: trace back to find the actual solution of the set of equation. 
-- This will work on the triangle matrix that had been flipped for easier recursion.
doBack:: Matrix -> Vector
doBack [] =[]
doBack (r:rs) =x: (doBack rs')
 where x=head r `div` last r
       rs'=map (\xs -> (head xs - x*  (head  (tail xs))) : (tail (tail xs))) rs

-- Quick Test   
d=reverse $ doBack c

-- solve: link all function together. Order: perform gaussian elim to get triangle matrix
-- -> flip it vertically and do doBack to seek for actual solution,
-- flip it again to get the solution in the right order
solve:: Matrix -> Vector
solve b = reverse $ doBack (reverse $map reverse (gaussElim b))

-- Credits: I would like to send a big Thanks to Dr. Sarah Tanner's notes in module Mathematics 1 
-- for being a concrete origin of the algorithm and which allowed me to build this program, 
-- and Dr. Fiona Lawless's guidance in linear algebra and how to apply Mathematics procedures 
-- in programming environment







--O= n^2 + (n-1)**2 + (n-2) **2+....+1

